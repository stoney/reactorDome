# reactor java dome code



## Reactor-RxJava-CompletableFuture 比较


| Reactor3       | RxJava1.x         | CompletableFuture | 描述|
|:------------- |:---------------| :-------------| :-------------|
|create,from,just|create,from,just | runAsync,supplyAsync|创建流,异步|
| map  | map | thenApply  | 转换对象|
| map  | map | thenApplyAsync | 转换对象,使用ForkJoinPool|
| flatMap | flatMap | thenCompose  | 转换对象为Future或Observable |
| flatMap | flatMap | thenComposeAsync | 转换对象为Future或Observable,使用ForkJoinPool|
| combineLatest | combineLatest | thenCombine | 当2个都消费完成后，执行提供的fn,用来组合结果|
| combineLatest | combineLatest | thenCombineAsync | 当2个都消费完成后，执行提供的fn,用来组合结果,使用ForkJoinPool|
| combineLatest | combineLatest | thenBoth | 和combine类似，返回Future或Observable |
| combineLatest | combineLatest | thenBothAsync | 和combine类似，返回Future或Observable,使用ForkJoinPool|
| doOnComplete |doOnCompleted | whenComplete | 消费完成时对结果处理|
| doOnComplete| doOnCompleted | whenCompleteAsync | 消费完成时对结果处理,使用ForkJoinPool|
| doAfterTerminate | doAfterTerminate | handle | 消费完成时或者有异常时执行|
| doAfterTerminate | doAfterTerminate | handleAsync | 消费完成时或者有异常时执行,使用ForkJoinPool|
| subscribe | subscribe | thenAccept | 消费执行|
| subscribe | subscribe | thenAcceptAsync | 消费执行,使用ForkJoinPool|

## 构建异步流
  #### 1. callable构建：
  ```
    Observable.flatMap(streamEvent -> {
                return Observable.fromCallable(() -> {
                    streamEvent.done = true;
                    dispatch.doWork(streamEvent);
                    return streamEvent;
                }).subscribeOn(Schedulers.io());
     });
  ```
  #### 2. 直接构建：
    ```
      Observable.flatMap(streamEvent -> {
              return Observable.just(streamEvent).observeOn(Schedulers.io()).map(event1 -> {
                  event1.done = true;
                  dispatch.doWork(event1);
                  return event1;
              });
      });
    ```
    #### 3. 分组构建：
    ```
        Observable.groupBy(streamEvent -> streamEvent.type).flatMap(1|2);
    ```