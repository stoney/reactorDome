package com.stony.reactor;

import org.junit.Test;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.core.publisher.TopicProcessor;

import static reactor.bus.selector.Selectors.$;
import static reactor.bus.selector.Selectors.*;

/**
 * <p>orcTest
 * <p>com.stony.orc
 *
 * @author stony
 * @version 下午4:09
 * @since 2018/1/6
 */
public class EventBusTest {

    @Test
    public void test_bus(){

        EventBus bus = EventBus.create(TopicProcessor.create());

        //发布订阅模型
        bus.on($("topic"), (Event<String> ev) -> {
            String s = ev.getData();
            System.out.printf("Got %s on thread %s%n", s, Thread.currentThread());
        });
        bus.notify("topic", Event.wrap("Hello World!"));

        System.out.println("--------------");
        //请求应答模式
        bus.on($("reply.sink"), ev -> {
            System.out.printf("Got %s on thread %s%n", ev, Thread.currentThread()); //3
        });
        bus.receive($("job.sink"), this::doWork); //2
        bus.send("job.sink", Event.wrap("Hello World!", "reply.sink")); //1

        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        bus.getProcessor().onComplete();
    }

    private Object doWork(Event<?> ev) {
        System.out.printf("Receive %s on thread %s%n", ev, Thread.currentThread());
        return ev.getData().toString().toUpperCase();
    }
}
