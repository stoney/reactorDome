package com.stony.reactor;

import com.google.common.util.concurrent.ListenableFutureTask;
import org.junit.Test;
import rx.Observable;
import rx.observables.AsyncOnSubscribe;
import rx.schedulers.Schedulers;

import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

/**
 * <p>reactorDome
 * <p>com.stony.reactor
 *
 * @author stony
 * @version 下午9:53
 * @since 2018/1/6
 */
public class RxGuavaTest {

    @Test
    public void test_14() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        AtomicLong index = new AtomicLong();
        Observable<ByteBuffer> bytes = Observable.create(AsyncOnSubscribe.createStateful(
                () -> 0L,
                (waterMark, requestAmount, observableObserver) -> {
                    long start = requestAmount;
                    long end = waterMark + requestAmount;
                    System.out.println("start = " + start + ",end = " + end);
                    ListenableFutureTask<ByteBuffer> future = ListenableFutureTask.create(() -> getData(start, end));
                    Observable<ByteBuffer> requestProducer = ListenableFutureObservable.from(future, Schedulers.computation());
                    observableObserver.onNext(requestProducer);
                    return end;
                }));

        bytes.subscribe(System.out::println);

        latch.await();
    }


    private static ByteBuffer getData(long start, long end) {
        // this is where you would produce the data (here we just allocate an arbitrary bytebuffer)
        return ByteBuffer.allocate((int) (end - start));
    }
}
