package com.stony.reactor;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * <p>reactorDome
 * <p>com.stony.reactor
 *
 * @author stony
 * @version 下午4:32
 * @since 2018/1/12
 */
public class IdGeneratorTest {

    @Test
    public void test_snow() {
        IdGenerator generator = new SnowflakeIdGenerator(0, 0);
        for (int i = 0; i < 10; i++) {
            long id = generator.nextId();
            System.out.println(id + " ： " + Long.toBinaryString(id));
        }

        long id = generator.nextId();
        long time = id >> 22;
        System.out.println(new Date(time));
    }

    @Test
    public void test_simple(){
        IdGenerator generator = new SimpleIdGenerator(0, 0);
        for (int i = 0; i < 10; i++) {
            long id = generator.nextId();
            System.out.println(id + " ： " + Long.toBinaryString(id));
        }

        long id = generator.nextId();
        long time = id >> 32;
        System.out.println(new Date(time*1000));
    }

    @Test
    public void test_46() throws UnknownHostException {
        System.out.println(InetAddress.getLocalHost());
        for (int i = 0; i < 100; i++) {
        }
    }
}
