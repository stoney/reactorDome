package com.stony.reactor;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import static java.time.ZoneOffset.*;
import static java.time.format.DateTimeFormatter.*;

import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;

/**
 * <p>reactorDome
 * <p>com.stony.reactor
 *
 * @author stony
 * @version 下午2:50
 * @since 2018/1/12
 */
public class SimpleByteBufferTest {

    @Test
    public void test_14(){
        SimpleByteBuffer buffer = new SimpleByteBuffer();
        buffer.writerInt(333);
        buffer.writerInt(444);

        System.out.println(buffer.readInt());
        System.out.println(buffer.readInt());

    }
}
