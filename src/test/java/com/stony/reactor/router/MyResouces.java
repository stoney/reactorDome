package com.stony.reactor.router;

import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.api.core.ScanningResourceConfig;
import com.sun.jersey.core.spi.scanning.PackageNamesScanner;

/**
 * <p>reactorDome
 * <p>com.stony.reactor.router
 *
 * @author stony
 * @version 下午6:58
 * @since 2018/1/18
 */
public class MyResouces extends ScanningResourceConfig {

    String pkgNamesStr = "com.stony.reactor.router";
    public MyResouces() {
        String[] pkgNames = getElements(new String[]{pkgNamesStr}, ResourceConfig.COMMON_DELIMITERS);

        init(new PackageNamesScanner(pkgNames));
    }
}
