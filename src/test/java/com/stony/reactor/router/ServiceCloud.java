package com.stony.reactor.router;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * <p>reactorDome
 * <p>com.stony.reactor.router
 *
 * @author stony
 * @version 下午7:03
 * @since 2018/1/18
 */
@Path("/cloud")
public class ServiceCloud {

    @GET
    public String get(){
        return "cloud";
    }

    @GET
    @Path("/v")
    public String v(){
        return "v";
    }


}