package com.stony.reactor.auth;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * <p>reactorDome
 * <p>com.stony.reactor.auth
 *
 * @author stony
 * @version 下午7:09
 * @since 2018/1/16
 */
public class QRTest {
    
    @Test
    public void test_14() throws WriterException, IOException {
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        String str = "otpauth://totp/shi@xx.com?secret=7AGOJT24234V6XVJM";
        BitMatrix bitMatrix = new MultiFormatWriter()
                .encode(str,
                BarcodeFormat.QR_CODE, 100, 100, hints); // 生成矩阵

        //将矩阵转为Image
        BufferedImage image = MatrixToImageWriter.toBufferedImage(bitMatrix);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        String fileName = this.getClass().getResource("/").getPath() + "/test.png";
        System.out.println(fileName);
        ImageIO.write(image, fileName, out);//将BufferedImage转成out输出流
        FileOutputStream oot = new FileOutputStream(new File(fileName));

        oot.write(out.toByteArray());

        oot.close();


        out.close();
    }
    
    @Test
    public void test_56() throws Exception {
        String fileName = this.getClass().getResource("/").getPath() + "/test.png";
        System.out.println(fileName);
        FileOutputStream stream = new FileOutputStream(new File(fileName));
        QRCodeWriter writer = new QRCodeWriter();
        String str = "otpauth://totp/shi@xx.com?secret=7AGOJT24234V6XVJM";
        BitMatrix m = writer.encode(str,
                BarcodeFormat.QR_CODE, 100, 100);
        MatrixToImageWriter.writeToStream(m, "png", stream);

        stream.flush();
        stream.close();
    }
    @Test
    public void test_68() throws Exception {

        HashMap hints = new HashMap();
        // 设置编码方式utf-8
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        //设置二维码的纠错级别为h
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

        String fileName = this.getClass().getResource("/").getPath() + "/test001.png";
        String str = "otpauth://totp/shi@xx.com?secret=7AGOJT24234V6XVJM"; // 二维码内容
        BitMatrix byteMatrix = new MultiFormatWriter().encode(new String(str.getBytes("UTF-8"),"iso-8859-1"),
                BarcodeFormat.QR_CODE, 200, 200);

        MatrixToImageWriter.writeToPath(byteMatrix, "png", Paths.get(fileName));
    }

    private static int width = 300;              //二维码宽度  
    private static int height = 300;             //二维码高度  
    private static int onColor  = 0xDD234231;     //前景色
    private static int offColor = 0xFFFFFFFF;    //背景色  
    private static int margin = 1;               //白边大小，取值范围0~4  
    private static ErrorCorrectionLevel level = ErrorCorrectionLevel.L;  //二维码容错率  
    
    @Test
    public void test_94() throws Exception{
        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
        // 指定纠错等级
        hints.put(EncodeHintType.ERROR_CORRECTION, level);
        // 指定编码格式
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.MARGIN, margin);   //设置白边


        String fileName = this.getClass().getResource("/").getPath() + "/test005.png";
        String str = "otpauth://totp/shi@xx.com?secret=7AGOJT24234V6XVJM"; // 二维码内容

        MatrixToImageConfig config = new MatrixToImageConfig(onColor, offColor);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, width, height, hints);

        MatrixToImageWriter.writeToPath(bitMatrix, "png", Paths.get(fileName), config);
    }

    @Test
    public void test_116() throws Exception {
        String logoPath = this.getClass().getResource("/").getPath() + "/logo.png";
        String fileName = this.getClass().getResource("/").getPath() + "/test006.png";
        String str = "otpauth://totp/shi@xx.com?secret=7AGOJT24234V6XVJM"; // 二维码内容
        generateQRImage(str, logoPath, fileName, "png");
    }

    public static void generateQRImage(String txt, String logoPath, String fileName, String suffix) throws Exception{

        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, level);
        hints.put(EncodeHintType.MARGIN, margin);  //设置白边
        BitMatrix bitMatrix = new MultiFormatWriter().encode(txt, BarcodeFormat.QR_CODE, width, height, hints);
        File qrcodeFile = new File(fileName);
        writeToFile(bitMatrix, suffix, qrcodeFile, logoPath);
    }


    /**
     * 生成带logo的二维码图片
     * @param txt          //二维码内容
     * @param logoPath     //logo绝对物理路径
     * @param imgPath      //二维码保存绝对物理路径
     * @param imgName      //二维码文件名称
     * @param suffix       //图片后缀名
     * @throws Exception
     */
    public static void generateQRImage(String txt, String logoPath, String imgPath, String imgName, String suffix) throws Exception{

        File filePath = new File(imgPath);
        if(!filePath.exists()){
            filePath.mkdirs();
        }

        if(imgPath.endsWith("/")){
            imgPath += imgName;
        }else{
            imgPath += "/"+imgName;
        }
        generateQRImage(txt, logoPath, imgPath, suffix);


    }
    /**
     *
     * @param matrix 二维码矩阵相关
     * @param format 二维码图片格式
     * @param file 二维码图片文件
     * @param logoPath logo路径
     * @throws IOException
     */
    public static void writeToFile(BitMatrix matrix,String format,File file,String logoPath) throws IOException {
        BufferedImage image = toBufferedImage(matrix);
        Graphics2D gs = image.createGraphics();

        int ratioWidth = image.getWidth()*2/10;
        int ratioHeight = image.getHeight()*2/10;
        //载入logo
        Image img = ImageIO.read(new File(logoPath));
        int logoWidth = img.getWidth(null)>ratioWidth?ratioWidth:img.getWidth(null);
        int logoHeight = img.getHeight(null)>ratioHeight?ratioHeight:img.getHeight(null);

        int x = (image.getWidth() - logoWidth) / 2;
        int y = (image.getHeight() - logoHeight) / 2;

        gs.drawImage(img, x, y, logoWidth, logoHeight, null);
        gs.setColor(Color.black);
        gs.setBackground(Color.WHITE);
        gs.dispose();
        img.flush();
        if(!ImageIO.write(image, format, file)){
            throw new IOException("Could not write an image of format " + format + " to " + file);
        }
    }

    public static BufferedImage toBufferedImage(BitMatrix matrix){
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        for(int x=0;x<width;x++){
            for(int y=0;y<height;y++){
                image.setRGB(x, y, matrix.get(x, y) ? onColor : offColor);
            }
        }
        return image;
    }

    public static BitMatrix deleteWhite(BitMatrix matrix){
        int[] rec = matrix.getEnclosingRectangle();
        int resWidth = rec[2] + 1;
        int resHeight = rec[3] + 1;

        BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);
        resMatrix.clear();
        for (int i = 0; i < resWidth; i++) {
            for (int j = 0; j < resHeight; j++) {
                if (matrix.get(i + rec[0], j + rec[1]))
                    resMatrix.set(i, j);
            }
        }
        return resMatrix;
    }
}
