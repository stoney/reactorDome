package com.stony.reactor.auth;

import com.stony.reactor.auth.TimeBasedOneTimePasswordUtil;
import org.junit.Test;

/**
 * <p>orcTest
 * <p>com.stony.orc
 *
 * @author stony
 * @version 下午2:15
 * @since 2018/1/4
 */
public class TimeBasedOneTimePasswordUtilTest {

    @Test
    public void test_14(){
        System.out.println(TimeBasedOneTimePasswordUtil.generateBase32Secret());
    }

    @Test
    public void test_21() throws Exception {
        String base32Secret = "4SWMEJSHKN2G6RPG";

        System.out.println("secret = " + base32Secret);


        // this is the name of the key which can be displayed by the authenticator program
        String keyId = "user@j256.com";
        // generate the QR code
        System.out.println("Image url = " + TimeBasedOneTimePasswordUtil.qrImageUrl(keyId, base32Secret));
        // we can display this image to the user to let them load it into their auth program


        String authUrl = TimeBasedOneTimePasswordUtil.generateOtpAuthUrl(keyId, base32Secret);
        System.out.println("authUrl = " + authUrl);
        // we can use the code here and compare it against user input
        String code = TimeBasedOneTimePasswordUtil.generateCurrentNumberString(base32Secret);

        System.out.println("code = " + code);
		/*
		 * this loop shows how the number changes over time
		 */
        while (true) {
            long diff = TimeBasedOneTimePasswordUtil.DEFAULT_TIME_STEP_SECONDS
                    - ((System.currentTimeMillis() / 1000) % TimeBasedOneTimePasswordUtil.DEFAULT_TIME_STEP_SECONDS);
            code = TimeBasedOneTimePasswordUtil.generateCurrentNumberString(base32Secret);
            System.out.println("Secret code = " + code + ", change in " + diff + " seconds");
            Thread.sleep(1000);
        }
    }
}
