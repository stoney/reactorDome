package com.stony.reactor;

import com.stony.reactor.jersey.ClassPathResourceConfig;
import com.stony.reactor.jersey.JacksonProvider;
import com.stony.reactor.jersey.JerseyBasedHandler;
import com.stony.reactor.router.MyResouces;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.NettyContext;
import reactor.ipc.netty.http.server.HttpServer;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

/**
 * <p>reactorDome
 * <p>com.stony.reactor
 *
 * @author stony
 * @version 下午6:55
 * @since 2018/1/18
 */
public class JerseyTest {
    @Test
    public void test_13(){

        MyResouces m = new MyResouces();
        System.out.println(m.getLanguageMappings());
        System.out.println(m.getRootResourceClasses());
    }

    @Test
    public void test_24(){

//        HttpServer.create(8080).startAndAwait(new JerseyBasedHandler("com.stony.reactor.router"));
        HttpServer.create(8080)
                .startAndAwait(JerseyBasedHandler.builder()
                        .withClassPath("com.stony.reactor.router")
                        .addValueProvider(JacksonProvider.class)
                        .build());

        System.out.println("---server start .....");

        System.out.println("--x---xxx");
        try {
            System.out.println("------onClose-----");
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            System.out.println("----dispose-------");
        }
    }
}
